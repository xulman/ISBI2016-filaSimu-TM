#ifndef GTGEN_RND_GENERATORS_H
#define GTGEN_RND_GENERATORS_H

#include "params.h"

/**
 * Generates random numbers that tend to form in Gaussian distribution
 * with given \e mean and \e sigma.
 *
 * \param[in] mean	mean of the distribution
 * \param[in] sigma	sigma of the distribution
 * \param[in] reseed	force generator to re-seed itself
 *
 * The function uses GSL random number generator:
 * http://www.gnu.org/software/gsl/manual/html_node/The-Gaussian-Distribution.html
 */
float GetRandomGauss(const float mean, const float sigma, const bool reseed=false);

/**
 * Generates random numbers that tend to form in uniform/flat distribution
 * within given interval \e A and \e B.
 *
 * \param[in] A		start of the interval
 * \param[in] B		end of the interval
 * \param[in] reseed	force generator to re-seed itself
 *
 * The function uses GSL random number generator:
 * http://www.gnu.org/software/gsl/manual/html_node/The-Flat-_0028Uniform_0029-Distribution.html
 */
float GetRandomUniform(const float A, const float B, const bool reseed=false);

/**
 * Generates random numbers that tend to form in Poisson distribution
 * with given \e mean.
 *
 * \param[in] mean	mean of the distribution
 * \param[in] reseed	force generator to re-seed itself
 *
 * The function uses GSL random number generator:
 * http://www.gnu.org/software/gsl/manual/html_node/The-Poisson-Distribution.html
 */
unsigned int GetRandomPoisson(const float mean, const bool reseed=false);

#endif
