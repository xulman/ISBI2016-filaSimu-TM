/*---------------------------------------------------------------------

sim_macros.h

This file is part of CytoPacq

Copyright (c) 2007-2012 Centre for Biomedical Image Analysis (CBIA)
                        Masaryk University

CytoPacq is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CytoPacq is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CytoPacq. If not, see <http://www.gnu.org/licenses/>.

Author: David Svoboda <svoboda@fi.muni.cz>

Description: 

	Shared configuration C/C++ header file. 
	It contains the definition of implicit input/output filenames. 
	It also defines simple debug interface. 
	All the definitions are realized via macros.

-------------------------------------------------------------------------*/

#ifndef _SIM_MACROS_
#define _SIM_MACROS_

#include <string.h>

/****************************************************************************/

// TODO: dodelat _SHORT_FILE_ i pro windows!!!

#define _SHORT_FILE_ strrchr(__FILE__, '/') ? \
		  strrchr(__FILE__, '/') + 1 : __FILE__

#ifdef DEBUG
	#define echo(x) std::cout << \
		  std::string(_SHORT_FILE_) << "::" << std::string(__FUNCTION__) << \
        "(): " << x << std::endl;
#else
	#define echo(x)
#endif

/****************************************************************************/

#define ERROR_DETAILS(x) std::string(_SHORT_FILE_)+"::"+__FUNCTION__+"()::"+(x)

/****************************************************************************/

#define TYPE_DESCRIPTION \
 cerr << "DESCRIPTION" << endl <<  \
	"\t A simulation toolbox developed by Centre for Biomedical\n" \
	"\t Image Analysis <http://cbia.fi.muni.cz/>. The software is\n" \
	"\t available for users working under Linux and Windows.\n" \
	"\t The project page: <http://cbia.fi.muni.cz/projects/simulator.html>." \
	<< endl << endl;

/****************************************************************************/

#define PHANTOM_NAME 		"image-phantom.ics"
#define LABELED_NAME			"image-labels.ics"
#define BLURRED_IMG_NAME 	"image-blurred.ics"
#define FINAL_NAME			"image-final.ics"
#define PLUGIN_DIR_NAME		"src/cytogen/plugins"
#define ERROR_LOG_NAME		"error.log"
#define WARNING_LOG_NAME	"warning.log"

/****************************************************************************/

#define DEG_TO_RAD(x) ((x)*M_PI/180)
#define SQR(x) ((x)*(x))

/****************************************************************************/

#ifdef _WIN32
	#define DLL_EXPORT __declspec(dllexport)
#else
	#define DLL_EXPORT
#endif

/****************************************************************************/

#endif

