/*---------------------------------------------------------------------

myrandom.cc

This file is part of CytoPacq

Copyright (C) 2007-2013 -- Centre for Biomedical Image Analysis (CBIA)

CytoPacq is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CytoPacq is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CytoPacq. If not, see <http://www.gnu.org/licenses/>.

Author: David Svoboda

Description: A simple (float) random number generator.

-------------------------------------------------------------------------*/

#include <math.h>
#include <stdlib.h>

#include "myrandom.h"

float GenNum(float mean, float var)
{
	 return mean * (1.0f + GenFromRange(-1,1)*var/100.0f);
}

float GenFromRange(float from, float to)
{
	return from + (to-from)*(((float)rand())/RAND_MAX);
}

