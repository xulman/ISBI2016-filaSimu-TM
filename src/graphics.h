#ifndef GRAPHICS_H
#define GRAPHICS_H


/**
 * Just inits the OpenGL stuff...
 */
void initializeGL(void);

/**
 * Just handles the idle loops... the OpenGL stuff...
 */
void loopGL(void);

/**
 * Just closes the OpenGL stuff...
 */
void closeGL(void);

#endif
