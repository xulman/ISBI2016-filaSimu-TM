The Texture Module (TM) only, meant to be published for the ISBI 2017.

It's a C++, CMake (and possibly OpenGL) project that compiles into a single
binary: ISBI_Generator 

The binary accepts two or three params:
1. path to a folder with VTK files
2. ID of the VTK file set
3. (optional parameter) time point from which it starts generating, default value is 0

The usage plan:

I've noticed Dima stores his simulations-experiments in folders IDxx
flatly somewhere in a FOLDER. All files within one experiment have a
certain naming pattern (e.g., ID53_t000_fTree02.vtk or ID53_t020_nucleus.vtk).
Also for my programming convenience, the TM module (the ISBI_Generator
binary) reads time points one by one and produces the final images
(anisotropic textures and masks). This is for me better as I do not need
to store/load my internal data to keep texture coherent, instead the
binary is executed once to do the whole job (all time points).

Thus, the binary should be executed as
/opt/ISBI_Generator FOLDER 53

which will read
FOLDER/ID53/ID53_t000_nucleus.vtk

if successful, will continue reading
FOLDER/ID53/ID53_t000_fTree01.vtk

if successful, will continue reading
FOLDER/ID53/ID53_t000_fTree02.vtk

if successful....

Notes:
If you want to study particular time point, use the third optional
parameter (to make it run from this time point) and rename the "later" nucleus
(to make it stop at that point, mv ID53_t047_nucleus.vtk ID53_t047_nucleus_BLOCK.vtk).

If you want to try another sequence, just change the second parameter.


Main developer and maintainer:
Vladimir Ulman (ulman@fi.muni.cz)