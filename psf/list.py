#!/usr/bin/python

locations = ["CBIA FI",
             "CBIA ILBIT",
	     "BFU"]

microscopes = ["Zeiss 200M",
               "Zeiss S100",
	       "Leica DMRXA",
	       "Leica DMIRE2"]

modes = ["confocal",
         "widefield"]

cameras = ["Andor iXon DV887",
           "Princeton Instruments Micromax 1300-YHS",
           "Photometrics CoolSNAP HQ",
	   "Photometrics Quantix KAF-1400",
	   "Photometrics CoolSNAP HQ2"]

objectives = ["Zeiss 10x/0.30",
              "Zeiss 20x/0.50",
	      "Zeiss 40x/0.75",
	      "Zeiss 40x/1.30 Oil",
	      "Zeiss 63x/1.30 Water-Glycerol",
	      "Zeiss 100x/1.45 Oil",
	      "Zeiss 63x/1.40 Oil",
	      "63x/1.30 Oil",
	      "Obecny",
	      "Zeiss 63x/1.40 Oil DIC (new)"]

excitation_filters = ["488/6 (488 Laser)",
                      "567/15 (568 Laser)",
		      "650/13 (648 Laser)",
		      "445/50 (DAPI)",
		      "470/40 (EGFP)",
		      "FRET (FITC/Cy3) cube",
		      "550/25 (Cy3)",
		      "640/30 (Cy5)",
		      "DAPI",
		      "FITC",
		      "Cy3",
		      "Cy5",
		      "blue(488nm) Ar/Kr Laser",
		      "yellow(568nm) Ar/Kr Laser",
		      "red(647nm) Ar/Kr Laser",
		      "405 Laser",
		      "488 Laser",
		      "561 Laser",
		      "640 Laser"]

emission_filters = ["475/50 (DAPI)",
                    "528/38 (FITC)",
		    "617/73 (Cy3)",
		    "685/40 (Cy5)",
		    "440/521/607/700 (QUAD)",
		    "488/568 (DUAL)",
		    "365 (DAPI)",
		    "525/50 (EGFP)",
		    "FRET (FITC/Cy3) cube",
		    "605/70 (Cy3)",
		    "690/50 (Cy5)",
		    "DAPI",
		    "FITC",
		    "Cy3",
		    "Cy5",
		    "Green HQ525/50",
		    "Red HQ615/60",
		    "Far red HQ700/75",
		    "Yokogawa tripple"]

glasses = ["1.0",
           "1.5"]

immersion_mediums = ["Zeiss Immersol 518F",
                     "Water"]

mediums = ["Vectashield",
           "Slowfade"]

temperatures = ["23 C",
                "37 C"]

confocal_units = ["none",
                  "Yokogawa CSU-10",
		  "Atto CARV"]

adapters = ["none (mag. 1x)",
            "0.63x",
	    "0.5x",
	    "1.25x"]

import sys

argv_parts = sys.argv[1].split('_')
argv_parts[len(argv_parts) - 1]=argv_parts[len(argv_parts) - 1][:-4]

# kontrolni vypis
#for i in range(0,len(argv_parts)):
# print `i` + " => " + argv_parts[i]

months = {"01":"January",
          "02":"Ferbuary",
          "03":"March",
          "04":"April",
          "05":"May",
          "06":"June",
          "07":"July",
          "08":"August",
          "09":"September",
          "10":"October",
          "11":"November",
          "12":"December"}

print "Parameters for the file: '" + sys.argv[1] + "' are following:"
print "  Date: " + months[argv_parts[0][5:7]] + " " + argv_parts[0][8:] + \
", " + argv_parts[0][:4]

print "  Location: " + locations[int(argv_parts[1])]
print "  Microscope: " + microscopes[int(argv_parts[2])]
print "  Objective: " + objectives[int(argv_parts[3])]
print "  Mode: " + modes[int(argv_parts[4])]
print "  Confocal unit: " + confocal_units[int(argv_parts[5])]
print "  Microscope-Unit adapter: " + adapters[int(argv_parts[6])]
print "  Unit-Camera adapter: " + adapters[int(argv_parts[7])]
print "  Camera: " + cameras[int(argv_parts[8])]
print "  Medium: " + mediums[int(argv_parts[9])]
print "  Immersion medium: " + immersion_mediums[int(argv_parts[10])]
print "  Temperature: " + temperatures[int(argv_parts[11])]
print "  Glass: " + glasses[int(argv_parts[12])]
print "  Excitation filter: " + excitation_filters[int(argv_parts[13])]
print "  Emission filter: " + emission_filters[int(argv_parts[14])]

